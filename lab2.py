import time
import unittest
import random
import matplotlib.pyplot as plt

'''
The below code contains the merge and insertion sort.
The class Lab2 can inherit the unittest.TestCase to check the testCase which is not shown in the code whatsoever
The first function is mergeSort, followed by insertionSort and two of them are for unittest. The last two are for plotting the graph
'''

class Lab2():
    #the merge sort is implemented here
    def mergeSort(self,arr):
        if len(arr) >1:
            mid = len(arr)//2
            L = arr[:mid]
            R = arr[mid:]

            self.mergeSort(L)
            self.mergeSort(R)

            i = j = k = 0

            while i < len(L) and j < len(R):
                if L[i] < R[j]:
                    arr[k] = L[i]
                    i+=1
                else:
                    arr[k] = R[j]
                    j+=1
                k+=1

            # Checking if any element was left
            while i < len(L):
                arr[k] = L[i]
                i+=1
                k+=1

            while j < len(R):
                arr[k] = R[j]
                j+=1
                k+=1
    def insertionSort(self,arr):
          # The code will walkthrough you for insertionSort.
        for i in range(1, len(arr)):

            key = arr[i]

            # Move elements of arr[0..i-1], that are
            # greater than key, to one position ahead
            # of their current position
            j = i-1
            while j >= 0 and key < arr[j] :
                    arr[j + 1] = arr[j]
                    j -= 1
            arr[j + 1] = key

    '''
    test_checkMergeSort,test_checkInsertionSort are used for checking for validity
    They resulted OKAY, which means our outcome is OKAY, but caution, this doesn't gurantee that the algorithm  is implemented well. They just make sure the result is OK
    '''
    def test_checkMergeSort(self):
        values = [5,1,3,4,2]
        self.mergeSort(values)
        toBeValue = [1,2,3,4,5]
        self.assertListEqual(values,toBeValue)

    def test_checkInsertionSort(self):
        values = [5,1,3,4,2]
        self.insertionSort(values)
        toBeValue = [1,2,3,4,5]
        self.assertListEqual(values,toBeValue)

    # this function uses the two function of mergeSort and InsertionSOrt we have used.
    def getDict(self,merge=True):
        randomList = random.sample(range(10000),10000)
        batch_size = 1000
        iter_range = int(len(randomList)/batch_size)
        length_time_dict = {}
        for i in range(iter_range):
            randomList = randomList[batch_size:]
            for i in range(100):
                random.shuffle(randomList)
            start = time.time()
            if merge:
                self.mergeSort(randomList)
            else:
                self.insertionSort(randomList)
            time_taken = time.time()-start
            length_time_dict[ len(randomList)]= time_taken
        lengthArray= []
        timeTaken=[]
        for i in sorted(length_time_dict.keys()):
            lengthArray.append(i)
            timeTaken.append(length_time_dict[i])
        return lengthArray,timeTaken

    #THe pLOTTING I S DONE FROM HERE
    def plotGraph(self):
        length_merge,time_merge = self.getDict()
        length_insertion,time_insertion =self.getDict(merge=False)

        plt.subplot(2,1,1)
        plt.plot(length_merge,time_merge)
        plt.title("MERGE  SORT")
        plt.legend()
        plt.subplot(2,1,2)
        plt.plot(length_insertion,time_insertion)
        plt.legend()
        plt.show()

Lab2().plotGraph()

